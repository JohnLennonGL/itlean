package com.jlngls.itlean.ui.Servicos;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.heinrichreimersoftware.materialintro.app.IntroActivity;
import com.heinrichreimersoftware.materialintro.slide.FragmentSlide;
import com.heinrichreimersoftware.materialintro.slide.SimpleSlide;
import com.jlngls.itlean.R;

public class SelecaoServicosActivity extends IntroActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_selecao_servicos);


        setButtonBackVisible( false );
        setButtonNextVisible( false );


        addSlide(new FragmentSlide.Builder()
                .background(android.R.color.white)
                .fragment(R.layout.chatbot_intro_1)
                .build());
        addSlide(new FragmentSlide.Builder()
                .background(android.R.color.white)
                .fragment(R.layout.chatbot_intro_2)
                .build());

        addSlide(new FragmentSlide.Builder()
                .background(android.R.color.white)
                .fragment(R.layout.chatbot_intro_3)
                .build());

        addSlide(new FragmentSlide.Builder()
                .background(android.R.color.white)
                .fragment(R.layout.chatbot_intro_4)
                .build());



//        addSlide( new SimpleSlide.Builder()
//                .title( "04\n" +
//                        "players de AI:\n" +
//                        "Google Cloud, IBM Watson,\n" +
//                        "Amazon Alexa e Microsoft LUIS" )
//                .image( R.drawable.botult )
//                .background( android.R.color.white )
//                .backgroundDark(R.color.colorPrimary )
//                .build() );
//
//        addSlide( new SimpleSlide.Builder()
//                .title( "Mais de\n" +
//                        "08\n" +
//                        "Incluindo: Messenger, Skype, Slack,\n" +
//                        "Telegram e Web"  )
//                .image( R.drawable.redessociais )
//                .background( android.R.color.white )
//                .backgroundDark(R.color.colorPrimary )
//                .build() );
//
//
//        addSlide( new SimpleSlide.Builder()
//                .title( "38\n" +
//                        "APIs integradas\n" +
//                        "em cloud" )
//                .image( R.drawable.apis )
//                .background( android.R.color.white )
//                .backgroundDark(R.color.colorPrimary )
//                .build() );


    }
}

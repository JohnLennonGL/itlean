package com.jlngls.itlean.ui.Servicos;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.jlngls.itlean.MainActivity;
import com.jlngls.itlean.R;
import com.jlngls.itlean.SplashActivity;
import com.jlngls.itlean.ui.Servicos.fragmentsolucoes.ChatBotFragment;

public class ServicosFragment extends Fragment {

    private ServicosViewModel slideshowViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        slideshowViewModel =
                ViewModelProviders.of(this).get(ServicosViewModel.class);
        View view = inflater.inflate(R.layout.fragment_servicos, container, false);



        final TextView textView = view.findViewById(R.id.text_slideshow);
        slideshowViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });



        return view;
    }


}

package com.jlngls.itlean;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Menu;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.jlngls.itlean.R;
import com.jlngls.itlean.ui.Servicos.SelecaoServicosActivity;
import com.jlngls.itlean.ui.Servicos.fragmentsolucoes.ChatBotFragment;

import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_principal, R.id.nav_servicos, R.id.nav_clientes, R.id.nav_contato,R.id.sobre, R.id.chatBot)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }




    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
// metodos on click do fab menu

    public void ligar(View view){

        String ligar = "tel:1155050409";
        Intent intent = new Intent(Intent.ACTION_DIAL,Uri.parse(ligar));
        startActivity(intent);
        Toast.makeText(this, "Encaminhando para Ligar", Toast.LENGTH_SHORT).show();
    }

    public void site(View view){

        String link = "https://itlean.com.br";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        startActivity(intent);

        Toast.makeText(this, "Encaminhando para o Site", Toast.LENGTH_SHORT).show();
    }

    public void endereco(View view){
    String link = "https://www.google.com/maps/place/IT+Lean/@-23.6118592,-46.6959308,17z/data=!3m1!4b1!4m5!3m4!1s0x94ce574bcfba7f27:0x52a8fa01b05344c3!8m2!3d-23.6118592!4d-46.6937421";
    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
    startActivity(intent);

        Toast.makeText(this, "Encaminhando para o Endereço", Toast.LENGTH_SHORT).show();
    }

    public void email(View view){

        Intent intent = new Intent(Intent.ACTION_SEND);

        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"contato@itlean.com.br"});
        intent.putExtra(Intent.EXTRA_SUBJECT,"Contato pelo App");

        intent.setType("message/rfc822");

        startActivity(Intent.createChooser(intent,"email"));

        Toast.makeText(this, "Encaminhando para o Email", Toast.LENGTH_SHORT).show();

}

public void onClickChatBot(View view){

//    ChatBotFragment chatBotFragment;
//
//    chatBotFragment = new ChatBotFragment();
//    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//    transaction.replace(R.id.selecaoActivity,chatBotFragment);
//    transaction.commit();

    Intent intent = new Intent(getApplicationContext(), SelecaoServicosActivity.class);
    startActivity(intent);



}






}

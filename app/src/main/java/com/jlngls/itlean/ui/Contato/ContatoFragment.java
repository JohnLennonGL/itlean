package com.jlngls.itlean.ui.Contato;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jlngls.itlean.R;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class ContatoFragment extends Fragment {

    private TextView link;


    public ContatoFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contato, container, false);

        link = view.findViewById(R.id.linkiitlean);
        link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                link();
            }
        });
        return view;


    }

    public void link(){

        String link = "https://itlean.com.br";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        startActivity(intent);
    }
}
